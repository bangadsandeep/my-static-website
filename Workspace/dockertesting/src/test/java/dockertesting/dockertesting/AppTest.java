package dockertesting.dockertesting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AppTest {

	//Count the number of names starting with alphabet A in list normal opps concept 
	//@Test
	public void regular()
	{
	ArrayList<String> names=new ArrayList<String>();
	names.add("Abhijeet");
	names.add("Don");
	names.add("Alekhya");
	names.add("dam");
	names.add("Ram");
	int count=0;
	
	for(int i=0;i<names.size();i++)
	{
		String actual=names.get(i);
		if(actual.startsWith("A"))
		{
			count++;
		}
	}

	System.out.println(count);	
		
		
	}


	//Count the number of names starting with alphabet A in list using streams
	//@Test
	public void StreamFilter()
	{
	ArrayList<String> names=new ArrayList<String>();
	names.add("Abhijeet");
	names.add("ADon");
	names.add("Alekhya");
	names.add("am");
	names.add("ARam");
	Long c = names.stream().filter(s->s.startsWith("A")).count();
	
	
	long d = Stream.of("Abc","SA","as","nhsd").filter(s->
	{
		s.startsWith("A"); 
		return true;
	}).count();
	//System.out.println(d);	
	
	//System.out.println(Stream.of("Abc","SA","as","nhsd").filter(s->s.startsWith("A")).count());
	
	//System.out.println(c);	
		
	//Print only thos which has lenght more than 4 characters
	//names.stream().filter(s->s.length()>4).forEach(s->System.out.println(s));
	
	//Limit single result on conditions
	names.stream().filter(s->s.length()>4).limit(1).forEach(s->System.out.println(s));
	
	
	
		
	}

	
	//uses of stream Map
	//@Test
	public void StreamMap()
	{
		//Print in upper case based on inputs 
		Stream.of("Abhijeet","DonDon","Alekhya","Don","Adam").filter(s->s.endsWith("a")).
			map(s->s.toUpperCase()).forEach(s->System.out.println(s)); 
	
		//Print name which have first letter as a with Upper case sorted
		
		List<String> names = Arrays.asList("Azbhijeet","Adam","Alekhya","Don","Adam");
		
		names.stream().filter(s->s.startsWith("A")).sorted().map(s->s.toUpperCase()).
		forEach(s->System.out.println(s));
				
	}



//uses of stream Map
//@Test
public void Matchtwostream()
{
	List<String> names = Arrays.asList("Azbhijeet","Adam","Alekhya","Don","Adam");
	
	
	List<String> names1 = Arrays.asList("man","woamne","ghghg","Don","Adam");
	//Merge two streams
	Stream <String>newstream = Stream.concat(names.stream(), names1.stream());
	//newstream.sorted().forEach(s->System.out.println(s));
	
	System.out.println(newstream.anyMatch(s->s.equalsIgnoreCase("adam")));
			
}



//uses of StreamCollect
@Test
public void StreamCollect()
{
	/*List<String> newlist = Stream.of("Azbhijeet","Adam","Alekhya","Don","Adam").filter(s->s.endsWith("m")).map
	(s->s.toUpperCase()).collect(Collectors.toList());
	
	System.out.println(newlist.get(0));*/
	
	
	List<Integer> Values = Arrays.asList(1,2,9,4,1,5,2,6,8,9);
	//Print Unique numbers
	//Values.stream().distinct().forEach(s->System.out.println(s));
	
	//Sort the array
	//Values.stream().sorted().forEach(s->System.out.println(s));
	
	//Sort the array - 3rd Index
	List<Integer> li = Values.stream().distinct().sorted().collect(Collectors.toList());
	
	System.out.println(li.get(2));
	
	//Rahul online tutorials 
	
	//mentor@rahulshettyacademy.com
	
}
}

